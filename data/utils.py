import numpy as np
import torch
import torch.nn.functional as F
def generate_text(model, char2idx, idx2char, num_generate=1000):
    start_string = "Romeo"
    input_eval = torch.tensor(np.array([char2idx[c] for c in start_string])).long()
    input_eval = input_eval.unsqueeze(0).cuda()

    text_generated = []

    for i in range(num_generate):
        outputs = model(input_eval).squeeze().cpu()

        if i > 0:
            prediction = torch.multinomial(F.softmax(outputs), num_samples=1)
        if i == 0:
            prediction = torch.multinomial(F.softmax(outputs), num_samples=1)[-1]

        text_generated.append(idx2char[prediction.cpu().numpy()])

        input_eval = prediction.unsqueeze(0).cuda()

    text_generated = list(np.hstack(text_generated))

    return text_generated


def read_and_preprocess_text(path_to_file):
    """
    :param path_to_file: Path to text file
    :return:
            text_as_int :numpy array of type int encoding the text
            char2idx: dict object mapping characters to indexes
            idx2char : dict object mapping indexes to characters
    """
    text = open(path_to_file, 'rb').read().decode(encoding='utf-8')

    print('Length of text: {} characters'.format(len(text)))

    vocab = sorted(set(text))
    vocab_size = len(vocab)
    print('{} unique characters'.format(vocab_size))

    # Creating a mapping from unique characters to indices
    char2idx = {u: i for i, u in enumerate(vocab)}
    idx2char = np.array(vocab)

    text_as_int = np.array([char2idx[c] for c in text])

    return text_as_int, char2idx, idx2char
