import numpy as np
import torch
import torch.nn as nn
import random
import torch.nn.functional as F
from torchvision import datasets, transforms
from random import Random
from torch.utils.data import Dataset, DataLoader, random_split
from .utils import read_and_preprocess_text


class ToyDataset(Dataset):
    def __init__(self, dimension, n_samples, sigma=0.2, seed=42):
        self.dimension = dimension
        self.n_samples = n_samples
        self.seed = seed
        np.random.seed(seed)
        self.X = np.random.randn(self.n_samples, self.dimension)
        self.y = (self.X.sum(axis=1) > 0).astype(np.uint8)
        noise = sigma*np.random.randn(n_samples, dimension)
        self.X = self.X + noise

        self.X = torch.tensor(self.X).float()
        self.y = torch.tensor(self.y).float()

    def __len__(self):
        return self.n_samples

    def __getitem__(self, idx):
        return self.X[idx], self.y[idx]


"""""""""""""""""""""""""""""""""""
           MNIST
"""""""""""""""""""""""""""""""""
class Partition(torch.utils.data.Dataset):
    """ Dataset-like object, but only access a subset of it. """

    def __init__(self, data, index):
        self.data = data
        self.index = index

    def __len__(self):
        return len(self.index)

    def __getitem__(self, index):
        data_idx = self.index[index]
        return self.data[data_idx]

class DataPartitioner(object):
    """ Partitions a dataset into different chuncks. """

    def __init__(self, data, sizes=[0.7, 0.2, 0.1], seed=1234):
        self.data = data
        self.partitions = []
        rng = Random()
        rng.seed(seed)
        data_len = len(data)
        indexes = [x for x in range(0, data_len)]
        rng.shuffle(indexes)

        for frac in sizes:
            part_len = int(frac * data_len)
            self.partitions.append(indexes[0:part_len])
            indexes = indexes[part_len:]

    def use(self, partition):
        return Partition(self.data, self.partitions[partition])


class MyDataSet(torch.utils.data.Dataset):
    def __init__(self):
        self.mnist = datasets.MNIST(
            '../data/mnist',
            train=True,
            download=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize((0.1307, ), (0.3081, ))
            ]))

    def __getitem__(self, index):
        data, target = self.mnist[index]
        return data, target
  
    def __len__(self):
        return len(self.mnist)


def Load_MNIST(bsz,rank,size):
    dataset = MyDataSet()
    testset = torch.utils.data.DataLoader(
        datasets.MNIST('../data/mnist',train=False, transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,),(0.3081,))
                      ])),
        batch_size = bsz, shuffle = True)
    partition_sizes = [1.0 / size for _ in range(size)]
    partition = DataPartitioner(dataset, partition_sizes)
    partition = partition.use(rank)
    train_set = torch.utils.data.DataLoader(
        partition, batch_size=bsz, shuffle=True)
    return train_set, testset


class CharacheterDataset(torch.utils.data.Dataset):
    def __init__(self, text_as_int, seq_length):
        self.seq_length = seq_length
        self.text_as_int = text_as_int

    def __len__(self):
        return len(self.text_as_int) // (self.seq_length + 1)

    def __getitem__(self, idx):
        input_ = torch.tensor(self.text_as_int[idx: idx + self.seq_length])
        target = torch.tensor(self.text_as_int[idx + 1: idx + self.seq_length + 1])

        return input_, target


def Load_NLP(bz, rank, size, seq_len=7):
    path_to_text = "../data/nlp/shakespeare.txt"
    text_as_int, char2idx, idx2char = read_and_preprocess_text(path_to_text)
    dataset = CharacheterDataset(text_as_int, seq_len)

    train_size = int(len(dataset) * 0.7)
    trainset, testset = random_split(dataset, [train_size, len(dataset) - train_size])

    partition_sizes = [1.0 / size for _ in range(size)]
    partition = DataPartitioner(trainset, partition_sizes)
    partition = partition.use(rank)
    train_set = torch.utils.data.DataLoader(
        partition, batch_size=bz, shuffle=True)
    testset = torch.utils.data.DataLoader(
        testset, batch_size=bz, shuffle=False)
 

    return train_set, testset, char2idx, idx2char



""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""Non IID"""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""


class DataPartitioner_Heter2(object):

    """ Partitions a dataset into different chuncks. """
    def __init__(self, data, seed=1234):
        self.data = data
        self.partitions = []
        rng = Random() 
        rng.seed(seed)
        data_len = len(data)
        # rng.shuffle(indexes)
 
        x = torch.utils.data.DataLoader(data, batch_size=1, shuffle=False) 
        notedown = []      
        for i in range(10):
            notedown.append([])
        counter = 0
        for data,target in x:
            # print(target)
            notedown[int(target.numpy())].append(counter)
            counter = counter+1 
        # print(len(notedown[2]))
        for i in range(10):
            datafor = notedown[i]+notedown[(i+1)%10]
            random.shuffle(datafor)
            self.partitions.append(datafor)

    def use(self, partition):
        return Partition(self.data, self.partitions[partition])


def Load_MNIST_Heter2(batchsize,rank,size):
    bsz = batchsize
    dataset = MyDataSet()
    testset = torch.utils.data.DataLoader(
        datasets.MNIST('../data/mnist',train=False, transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,),(0.3081,))
                      ])),
        batch_size = bsz, shuffle = False)
   
    partition = DataPartitioner_Heter2(dataset)
    get_number = rank
    partition = partition.use(get_number)
    train_set = torch.utils.data.DataLoader(
        partition, batch_size=bsz, shuffle=False)
    print(f"Rank {rank} get two numbers {rank} and {(rank+1)%10} Number of batches {len(train_set)}")

    return train_set, testset


def LoadData(name = "Toy", bsz = 128, size = 4, rank = 0):
    if name == "Toy":
        n_samples = 10000
        n_test_samples = 1000
        train_toy_dataset = ToyDataset(10, n_samples, sigma = 0.1)
        test_toy_dataset = ToyDataset(10, n_test_samples, sigma = 0.1)

        lengths = [n_samples//size for _ in range(size)]
        if size!=1: lengths[-1] = n_samples-sum(lengths[0:-1])
        torch.manual_seed(train_toy_dataset.seed)
        local_toy_datasets = random_split(train_toy_dataset, lengths=lengths)
        train_loader = [DataLoader(local_data, shuffle=True, batch_size=bsz) for local_data in local_toy_datasets]
        test_loader = DataLoader(test_toy_dataset, shuffle=False, batch_size=bsz)

        return train_loader[rank], test_loader
    elif name == "Mnist" :
        return Load_MNIST(bsz, rank, size)
    elif name == "MnistHeter" :
        return Load_MNIST_Heter2(bsz, rank, size)

    elif name == "NLP":
        return Load_NLP(bsz, rank, size)

    else:
        print("Warning: No such data set")  
        exit(0)




"""""""""""""Model"""""""""""""""""""



class ToyModel(nn.Module):
    def __init__(self, input_dimension):
        super(ToyModel, self).__init__()
        self.input_dimension = input_dimension
        self.fc = nn.Linear(input_dimension, 1)

    def forward(self, x):
        return torch.sigmoid(self.fc(x))

class SimpleMnist(nn.Module):
    def __init__(self):
        super(SimpleMnist,self).__init__()
        self.conv1 = nn.Conv2d(1,20,5,1)
        self.conv2 = nn.Conv2d(20,50,5,1)
        self.fc1 = nn.Linear(4*4*50,500)
        self.fc2 = nn.Linear(500,10) 
    def forward(self,x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x,2,2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x,2,2)
        x = x.view(-1,4*4*50)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x

class SimpleLSTM(nn.Module):
    def __init__(self, vocab_size=66, embedding_dim=256, hidden_dim=128):
        super(SimpleLSTM, self).__init__()
        self.hidden_dim = hidden_dim

        self.char_embeddings = nn.Embedding(vocab_size, embedding_dim)

        self.lstm = nn.RNN(embedding_dim, hidden_dim, batch_first=True)

        # The linear layer that maps from hidden state space to tag space
        self.hidden2char = nn.Linear(hidden_dim, vocab_size)

    def forward(self, sentence):
        embeds = self.char_embeddings(sentence)
        lstm_out, _ = self.lstm(embeds)
        char_space = self.hidden2char(lstm_out)
        char_scores = F.log_softmax(char_space, dim=2)
        return char_scores.permute(0, 2, 1)


def LoadModel(name = "Toy"):
    if name == "Toy":
        return ToyModel(10) 
    elif name == "Mnist" or name == "MnistHeter":
        return SimpleMnist()
    elif name =="NLP":         
        return SimpleLSTM()
    else: 
        print("Warning: No such a model!")
        exit(0)
