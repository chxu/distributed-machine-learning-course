import os
import random
import time
import numpy as np
import torch
import torch.distributed as dist
import torch.nn.functional as F
import torch.optim as optim
import sys
sys.path.append("..")
import data.prepare as data  
import data.parser as parser
from data.utils import generate_text

def train_one_epoch(model, loader, optimizer, criterion):
    model.train()
    running_loss = 0.0
    for bid, (inputs, targets) in enumerate(loader):
        optimizer.zero_grad()
        inputs, targets = inputs.cuda(), targets.cuda()
        outputs = model(inputs)

        loss = criterion(outputs.squeeze(), targets)

        loss.backward()
        optimizer.step()

        running_loss += loss.item()
        break

    return running_loss/len(loader) #approximate local loss 


def test_mnist(model, test_set):
    model.eval()
    test_loss = 0
    correct = 0
    itera = 0
    with torch.no_grad():
        for data, target in test_set:
            data, target = data.cuda(), target.cuda()
            itera +=1
            output = model(data)
            lossfunction = torch.nn.CrossEntropyLoss()
            test_loss += lossfunction(output, target)
            pred = output.max(1, keepdim= True)[1]
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= itera
    acc = correct/len(test_set.dataset)
    return test_loss.item(), acc


def test_nlp(model, test_set):
    model.eval()
    test_loss = 0
    correct = 0
    itera = 0
    with torch.no_grad():
        for data, target in test_set:
            data, target = data.cuda(), target.cuda()
            itera +=1
            output = model(data)
            lossfunction = torch.nn.CrossEntropyLoss()
            test_loss += lossfunction(output, target)
            pred = output.max(1, keepdim= True)[1]
            correct += pred.eq(target.view_as(pred)).sum().item()/7

    test_loss /= itera
    acc = correct/len(test_set.dataset)
    return test_loss.item(), acc


def test_mode(dataset, model, test_set, criteria):
    if dataset == "Toy":
        return test_toy(model, test_set, criteria)
    if dataset == "Mnist" or dataset == "MnistHeter":
        return test_mnist(model, test_set)
    if dataset == "NLP":
        return test_nlp(model, test_set)


def train_model(model, epochs, train_loader, test_loader, optimizer, criterion, rank=0, verbosity=0):
    for epoch in range(epochs):
        if verbosity:
            print("Clients {} starts epoch {}".format(rank, epoch))

        train_loss = train_one_epoch(model, train_loader, optimizer, criterion)
    return train_loss


def run(model, train_loader, optimizer, rank, round, local_epochs, criterion, verb=0):
    train_loss =\
        train_model(model, local_epochs, train_loader,
                test_loader, optimizer, criterion, rank, verbosity=0)


if __name__=="__main__":

    nbr_rounds = 100
    fraction = 1
    local_epochs = 1
    dataset = "NLP"
    # Launch parallel processes 
    dist.init_process_group(backend='gloo',init_method = "env://")
    rank = dist.get_rank()
    nbr_workers = dist.get_world_size()-1
 
    # Read Data 
    if rank !=0:
        if dataset == "NLP": 
            train_loader, test_loader , char2idx, idx2char = data.LoadData(name = dataset, bsz = 128, size = nbr_workers, rank=rank-1)
        else:
            train_loader, test_loader = data.LoadData(name = dataset, bsz = 128, size = nbr_workers, rank=rank-1)
    else:   
        if dataset == "NLP": 
            _, test_loader, char2idx, idx2char = data.LoadData(name = dataset, bsz = 128, size = nbr_workers, rank=rank-1)
        else: 
            _, test_loader = data.LoadData(name = dataset, bsz = 128, size = nbr_workers, rank=0)


    if torch.cuda.is_available() == True:
        torch.cuda.set_device(0)
    else: 
        print(f"Warining: There is no gpu availabel!!")
        exit()

    
    torch.manual_seed(1234) 
    ##### Read Model #####
    Model = data.LoadModel(name = dataset).cuda()

    ##### Choose Optimizer #####
    optimizer = optim.SGD(Model.parameters(), lr=0.01)


    # Choose the loss function
    if dataset == "Toy":
        criterion = F.binary_cross_entropy
    else: criterion = torch.nn.CrossEntropyLoss()
       
    for round_n in range(nbr_rounds):
        for param in Model.parameters():
            dist.broadcast(tensor=param.data,src=0)
        if rank != 0: 
            run(Model, train_loader, optimizer, rank,  round_n, local_epochs, criterion,  0)
        if True:
            for param in Model.parameters():
                if rank == 0: param_old = param.data.clone()
                dist.reduce(tensor=param.data, dst=0, op=dist.ReduceOp.SUM)
                if rank == 0:
                    param.data = param.data-param_old
                    param.data /= nbr_workers    
         
        if rank==0 and round_n%10 == 0: 
            loss, acc = test_mode(dataset, Model, test_loader, criterion)        
            print(f"Round {round_n} Test Loss {loss} Accuracy {acc}")

    if dataset == "NLP" and rank == 0:
        print("".join(generate_text(Model, char2idx, idx2char))) 

