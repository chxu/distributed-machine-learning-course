import torch
import torch.distributed as dist
import random
def run(rank, size):
    if rank==0: tensor = torch.tensor(1)
    else: tensor = torch.tensor(rank)
    fraction =0.5
    num_chosen_clients = max(int(fraction*size),1)
    random.seed(0)
    for t in range(3):
        selected_clients=random.sample([i for i in range(1,size)],num_chosen_clients)
        #print(f"{selected_clients}")
        group = dist.new_group(selected_clients+[0])
        dist.broadcast(tensor=tensor, src=0, group = group)
        #if rank==1: print(f"Rank {rank} Tensor {tensor}")
        if rank in selected_clients: tensor = tensor + torch.tensor(1) #Client updates its value

        if rank == 0: tensor_old = tensor.clone()
        dist.reduce(tensor=tensor, dst=0, group = group)
        if rank == 0: 
            tensor = (tensor-tensor_old)/(size-1)+tensor_old
            print(f"Iteration: {t+1}, Server: {tensor}")


if __name__ == "__main__":
    dist.init_process_group("gloo", init_method="env://")
    size = dist.get_world_size()
    rank = dist.get_rank()
    run(rank,size)
