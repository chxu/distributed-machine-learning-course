import os
import random
import time
import numpy as np
import torch
import torch.distributed as dist
import torch.nn.functional as F
import torch.optim as optim
import sys
sys.path.append("..")
import data.prepare as data  
import data.parser as parser


def train_one_epoch(model, loader, optimizer, criterion):
    model.train()
    running_loss = 0.0
    for bid, (inputs, targets) in enumerate(loader):
        optimizer.zero_grad()

        outputs = model(inputs)

        loss = criterion(outputs.squeeze(), targets)

        loss.backward()
        optimizer.step()

        running_loss += loss.item()

    return running_loss/len(loader)  # approximate local loss


def test_mode(model, loader, criterion):
    model.eval()
    running_loss = 0.0
    correct = 0.0
    for bid, (inputs, targets) in enumerate(loader):
        with torch.no_grad():
            outputs = model(inputs)
            loss = criterion(outputs.squeeze(), targets)
            running_loss += loss.item()

            correct += int(((outputs > 0.5).squeeze().float() == targets).sum())

    acc = correct / len(loader.dataset)

    return running_loss/len(loader), acc


def train_model(model, epochs, train_loader, test_loader, optimizer, criterion, rank=0, verbosity=0):
    for epoch in range(epochs):
        if verbosity:
            print("Clients {} starts epoch {}".format(rank, epoch))

        train_loss = train_one_epoch(model, train_loader, optimizer, criterion)
        test_loss, test_acc = test_mode(model, test_loader, criterion)

        if verbosity:
            print("Client {} finished epoch {} \
            train_loss = {:.4f} | test_loss = {:.4f}| test_acc = {:.5f}"\
            .format(rank, epoch, train_loss, test_loss, test_acc))

    return train_loss, test_loss, test_acc


def run(model, train_loader, optimizer, rank, round, local_epochs, verb=0):
    if verb >= 2:
        for param in model.parameters():
            print("(Begin) rank: {} | params: {}".format(rank, param.data))
    criterion = F.binary_cross_entropy 
    train_loss, test_loss, test_acc =\
        train_model(model, local_epochs, train_loader,
                test_loader, optimizer, criterion, rank, verbosity=0)

    if verb >= 2:        
        for param in model.parameters():
            print("(Middle) rank: {} | params: {}".format(rank, param.data))

    if verb >= 1 and rank == 1:    
        print("round: {} | rank: {} | train_loss={:.4f} | tesl_loss={:.4f} | acc = {:.4f}"\
                .format(round, rank, train_loss, test_loss, test_acc))
    # Calculate the loss here maybe
    if verb >= 2:
        for param in model.parameters():
            print("(End) rank: {} | params: {}".format(rank, param.data))


if __name__ == "__main__":
    nbr_rounds = 10
    fraction = 1
    local_epochs = 1
    # Launch parallel processes
    dist.init_process_group(backend='gloo',init_method = "env://")
    rank = dist.get_rank()
    nbr_workers = dist.get_world_size()-1
 
    ##### Read Data #####
    if rank !=0:
        train_loader, test_loader = data.LoadData(name="Toy", bsz=128, size=nbr_workers, rank=rank-1)
    else:   
        _, test_loader = data.LoadData(name="Toy", bsz=128, size=nbr_workers, rank=0)
    
    torch.manual_seed(1234) 
    ##### Read Model #####
    Model = data.LoadModel(name="Toy")

    ##### Choose Optimizer #####
    optimizer = optim.SGD(Model.parameters(), lr=0.01)

       
    for round_n in range(nbr_rounds):
        for param in Model.parameters():
            dist.broadcast(tensor=param.data, src=0)
        if rank != 0: 
            run(Model, train_loader, optimizer, rank,  round_n, local_epochs,  0)
        if True:
            for param in Model.parameters():
                if rank == 0: param_old = param.data.clone()
                dist.reduce(tensor=param.data, dst=0, op=dist.ReduceOp.SUM)
                if rank == 0:
                    param.data = param.data-param_old
                    param.data /= nbr_workers    
         
        if rank==0: 
            loss, acc = test_mode(Model, test_loader, F.binary_cross_entropy)        
            print(f"Round {round_n} Test Loss {loss} Accuracy {acc}")

   

