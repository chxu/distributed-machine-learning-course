import random
import numpy as np
import torch
import torch.nn.functional as F
import torch.optim as optim
import sys
from torch.autograd import Variable
sys.path.append("..")
import data.prepare as data  
def test_toy(model, loader, criterion):
    model.eval()
    running_loss = 0.0
    correct = 0.0
    for bid, (inputs, targets) in enumerate(loader):
        with torch.no_grad():
            outputs = model(inputs)
            loss = criterion(outputs.squeeze(), targets)
            running_loss += loss.item()

            correct += int(((outputs > 0.5).squeeze().float() == targets).sum())

    acc = correct / len(loader.dataset)

    return running_loss/len(loader), acc


def test_mnist(model, test_set):
    model.eval()
    test_loss = 0 
    correct = 0
    itera = 0
    with torch.no_grad():
        for data, target in test_set:
            itera +=1
            data, target = Variable(data), Variable(target)
            output = model(data)
            lossfunction = torch.nn.CrossEntropyLoss()             
            test_loss += lossfunction(output, target)
            pred = output.max(1, keepdim= True)[1]
            correct += pred.eq(target.view_as(pred)).sum().item()
    
    test_loss /= itera 
    test_error = 1-correct/len(test_set.dataset)
    return test_loss.item(), test_error

def test_mode(dataset, model, test_set, criteria):
    if dataset == "Toy":
        return test_toy(model, test_set, criteria)
    if dataset == "Mnist" or "MnistHeter":
        return test_mnist(model, test_set)





if __name__=="__main__":

    dataset = "Mnist" #Choose among "Toy", "Mnist" and "MnistHeter"
    epochs = 10
    ##### Read Data #####
    train_loader, test_loader = data.LoadData(name = dataset, bsz = 512, size = 1, rank = 0)   
    
    ##### Read Model #####
    torch.manual_seed(1234)
    Model = data.LoadModel(name = dataset)

    ##### Choose Optimizer #####
    optimizer = optim.SGD(Model.parameters(), lr=0.01)

    ##### Choose Loss function #####
    if dataset == "Toy": criteria =  F.binary_cross_entropy
    elif dataset == "Mnist" or dataset == "MnistHeter": criteria = torch.nn.CrossEntropyLoss()

    Iteration_number = len(train_loader)
    for e in range(epochs):

        Model.train()
 
        iter = 0

        for bid,(inputs, targets) in enumerate(train_loader):

            optimizer.zero_grad()

            outputs = Model(inputs)

            loss = criteria(outputs.squeeze(), targets)

            loss.backward()

            optimizer.step()
 
            iter += 1

            if dataset == "Mnist" or dataset == "MnistHeter": 
                print(f"Epoch: {e}, Iteration: {iter}/{Iteration_number}, Local Loss: {loss.item()}")

        loss,acc = test_mode(dataset, Model, test_loader, criteria)

        print(f"Round {e}, Test Loss {loss}, Accuracy {acc}")
        

 
   

