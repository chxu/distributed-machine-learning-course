# Distributed Machine Learning Implementation Course

## Usage of cluster Nef
* Basic commands
    * log in account: ssh user@nef-devel.inria.fr
    * reserve cpu cores and open the interactive terminal: 
        * oarsub -l /nodes=1/core=2,walltime=2 -I (reserving 2 cores from one node for two hours)
        * oarsub -l /nodes=2/core=1,walltime=2 -I (reserving 2 nodes and every node one core for two hours)
    * load Pytorch (could copy the following commandes into a single [shell file](https://gitlab.inria.fr/chxu/distributed-machine-learning-course/blob/master/LoadPyTorch.sh) and lauch it):
        * module load conda/5.0.1-python3.6
        * conda create --name virt_pytorch
        * source activate virt_pytorch
        * module load cuda/9.2
        * module load cudnn/7.1-cuda-9.2
        * module load gcc/7.3.0
        * module load mpi/openmpi-2.0.0-gcc
        * module load pytorch/1.4.0

    
## Distributed Machine learning
* Frameworks and Algorithms

## Pytorch for Machine Learning
* Basic functions
* [Distributed package](https://pytorch.org/docs/stable/distributed.html) and [its basic usage](https://pytorch.org/docs/stable/distributed.html#distributed-basics)
* Multiples methods to launch parallel processes:
    * Launch the code with mpi: mpirun -n NUMBER_OF_PROCESSEURS python try_mpi.py
    * Lauch the code with [torch.distributed.lauch package](https://github.com/pytorch/pytorch/blob/master/torch/distributed/launch.py): python -m torch.distributed.launch --nproc_per_node=4 --nnode=1 try_gloo.py
    * Using Multiprocessing package inside the code: python try_gloo_multiprocessing.py

## Implementation 
* Centralized vs Distributed (computation time, communication time, number of workers)
* Develop distributed package for consensus-based machine learning
* Dataset partition effect (iid, non iid)
* 

